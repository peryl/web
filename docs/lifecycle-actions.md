---
id: lifecycle-actions
title: Lifecycle Actions
---

In Peryl we believe that simplicity is key factor to build web applications,
so we decided to support three most used lifecycle actions `_init`, `_mount` and `_unmount`.

## \_init

Is automatically dispatched when peryl app is initialized.

## \_mount

Useful, when you need to make some changes after app is successfully mounted on DOM.

## \_unmount

Afer app is unmounted, we can free some resources.

## Example

<iframe width="100%" height="600" src="//jsfiddle.net/ateamsolutions/tzu7691p/embedded/html,result/" allowfullscreen="allowfullscreen" allowpaymentrequest frameborder="0"></iframe>
