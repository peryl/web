---
id: actions
title: Actions with Payload
---

## 1. Dispatch Action with Custom Payload on User Click

Sometimes, dispatching an action with custom payload comes handy. For example, when we have
a list of items and we need to identify which item dispatched an action, we can
send item's `id` within actions' payload.

```ts
type Action = [DomEvent, ActionName, ActionData];

// example
// h("div.item.close", { on: ["click", "DELETE_ITEM", item.id] });
```

<iframe width="100%" height="600" src="//jsfiddle.net/ateamsolutions/kt8z6qcg/embedded/html,result/" allowfullscreen="allowfullscreen" allowpaymentrequest frameborder="0"></iframe>

## 2. Dispatching Action with data from event

If you want to dispatch action with data from event (e.g. input's value), you
can get data directly from event and then dispatch.

```ts
type Action = [DomEvent, ActionName, (DomEvent) => ActionData];

// example
// h("input", { on: ["change", (e) => e.target.value] });
```

<iframe width="100%" height="600" src="//jsfiddle.net/ateamsolutions/egc5wkpn/embedded/html,result/" allowfullscreen="allowfullscreen" allowpaymentrequest frameborder="0"></iframe>
