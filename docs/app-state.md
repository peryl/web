---
id: app-state
title: Application State
---

In this section, we will learn how to update the application state and rerender the application.

One of Peryl features is to let the user manually rerender application, which is very handy when it comes
to mutating state, dealing with asynchronous actions (like http request) and preparing
for a bigger update. User can easily mutate application state and then evoke `update`
function, which will diff application's state against actual DOM and makes necessary DOM updates.

_Note: it is very easy to replicate similar behavior to React, where an application is automatically updated when the state is changed_

## 1. Using State

First thing we can see in example below is `const state = {};`, which is initial application's state that is passed to application.

<iframe width="100%" height="500" src="//jsfiddle.net/ateamsolutions/tjkcwz5b/embedded/html,result/" allowfullscreen="allowfullscreen" allowpaymentrequest frameborder="0"></iframe>

## 2. Display time every 2s

<iframe width="100%" height="600" src="//jsfiddle.net/ateamsolutions/td97kx30/embedded/html,result/" allowfullscreen="allowfullscreen" allowpaymentrequest frameborder="0"></iframe>
