---
id: http
title: HTTP Library
sidebar_label: HTTP Library
---

Peryl comes with custom HTTP lib with fluent api pattern. It is possbile to use peryl's http lib both on client and server.

## Using Http lib on client

```html
<script src="https://unpkg.com/peryl@1.4.30/dist/umd/http.js"></script>
<script>
  // Will make POST reqeust on current origin `${window.location.origin}/users/search`
  post("/users/search")
    .onResponse(function (r) {
      const responseData = r.getJson();
      console.log(responseData);
    })
    .onError(function (e) {
      console.error(e);
    })
    .send({
      name: "John Doe"
    });

  // Making a GET Request
  get("/users/31245-4235-532623")
    .onResponse(function (r) {
      const responseData = r.getJson();
      console.log(responseData);
    })
    .onError(function (e) {
      console.error(e);
    })
    .send();
</script>
```

## Using Http lib on server

```javascript
import * as Http from "peryl/dist/http";

get("www.google.com")
    .onResponse(function (r) {
      console.log(r.getText());
    })
    .onError(function (e) {
      console.error(e);
    })
    .send();
```

## Interface

### HTTP Request

```ts
export class HttpRequest {
    url(url: string, query?: Object): this;
    method(method: HttpMethod): this;
    headers(headers: { [key: string]: string }): this;
    use(middleware: (req: HttpRequest) => void): this;
    timeout(timeout: number): this;
    responseType(type: HttpResponseType): this;
    onProgress(onProgress: (progress: HttpProgress) => void): this;
    onResponse(onResponse: (response: HttpResponse) => void): this;
    onError(onError: (e?: Event) => void): this;
    async(async: boolean): this;
    noCache(noCache: boolean = true): this;
    abort(): this;
    send(data?: any, contentType?: string): void;
    sendPromise(data?: any, contentType?: string): Promise<HttpResponse>;
}
```

### HTTP Response

```ts
interface HttpResponse {
  getHeaders(): string;
  getHeader(header: string): string | null;
  getBody(): any;
  getType(): string;
  getContentType(): string | null;
  getText(): string;
  getJson(): any;
  getXml(): Document | null;
}
```