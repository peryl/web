---
id: resources
title: Resources
sidebar_label: Example Page
---

## Sources

Peryl repository [https://gitlab.com/peryl/peryl](https://gitlab.com/peryl/peryl)

Link to this documentation [https://gitlab.com/peryl/web](https://gitlab.com/peryl/web)

## Tools

Online Html to Hsml convertor [https://peryl.gitlab.io/html2hsml/](https://peryl.gitlab.io/html2hsml/)

Site generator using Peryl [https://gitlab.com/peryl/peryl-sitegen](https://gitlab.com/peryl/peryl-sitegen)

Peryl project template (SSR) [https://gitlab.com/peryl/project/](https://gitlab.com/peryl/project/)

PWA project template [https://gitlab.com/peryl/pwa](https://gitlab.com/peryl/pwa)

## References

Hyperscript Tools is based on [https://github.com/hyperhype/hyperscript](https://github.com/hyperhype/hyperscript)

Origin of HSML [http://www.jsonml.org/](http://www.jsonml.org/)
