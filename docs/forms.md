---
id: forms
title: Forms
sidebar_label: Forms
---

## Submitting form

First, we start with default html forms and how they work. When user fill
all inputs and then submits form (by using `submit` button or by pressing an `Enter`), it will send request to server with payload as `name/value` pairs. `name` is name of input and `value` is it's filled value by user. 

In example below, by submitting and form, client send form data as encoded payload (not a JSON !!!). In later example, we will show you how to send json as payload. 

<iframe width="100%" height="600" src="//jsfiddle.net/ateamsolutions/wgLbvq5x/embedded/html,result/" allowfullscreen="allowfullscreen" allowpaymentrequest frameborder="0"></iframe>

## Intercepting form events

From previous tutorial [Handling Events](events.md) we learn how to listen on events from inputs. We can apply same method on whole `form` instead on specif `input`s. To do so, just put `{ "on": ["change", "FORM_CHANGE"] }` listener on `form`.

**EVENTS:**

**change:** `action.data` contains only changed pairs (e.g. `{ username: "joe" }`)

**submit:** `action.data` contains all submited data (e.g. `{ username: "joe", email: "joe@mail.com" }`)

<iframe width="100%" height="600" src="//jsfiddle.net/ateamsolutions/t7Laz2ye/embedded/html,result/" allowfullscreen="allowfullscreen" allowpaymentrequest frameborder="0"></iframe>