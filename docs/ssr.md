---
id: ssr
title: Server Side Rendering
---

## Render template to a string

```typescript
import { hsmls2html } from "peryl/dist/hsml-html";
import { h } from "peryl/dist/hsml-h";

hsmls2html([
    h("html", [
        h("head", [
            h("link", { rel: "stylesheet", href: "https://www.w3schools.com/w3css/4/w3.css" }),
            h("title", "Example of SSR")
        ]),
        h("body", [
            h("div#app", [
                h("h1", "Hello World"),
                h("p", "This is example how to render HSML to a string")
            ])
        ])
    ])
], (html) => {
    // send to client
    console.log(html); // rendered hsml to html
});
```

## Example with Express

*NOTE: make sure you have installed [express](https://www.npmjs.com/package/express)*

```typescript
import * as express from "express";
import { hsmls2html } from "peryl/dist/hsml-html";
import { h } from "peryl/dist/hsml-h";

// first prepare basic template
function pageTemplate(title, content) {
    return [
        h("html", [
            h("head", [
                h("link", { rel: "stylesheet", href: "https://www.w3schools.com/w3css/4/w3.css" }),
                h("title", title)
            ]),
            h("body", content)
        ])
    ];
}

const app = express();
app.get("/", (req, res) => {
    hsmls2html(
        pageTemplate("Landing page", [
            h("h1", "Hello World"),
            h("p", "This is example of rendered hsml on server")
        ]), 
        (html) => {
            res.set("Content-Type", "text/html");
            res.write(html);
            res.end();
        }
    );
});

app.listen(3000, () => {
    console.log(`Example app listening at http://localhost:${port}`);
});
```