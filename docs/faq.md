---
id: faq
title: FAQ
---

## Why using `HSML` instead of `jsx` ?

We appreciate `jsx` and we worked with it a lot before, but we can also see several
drawbacks that come with using `jsx` like syntax. One of the biggest drawbacks from our
point of view is needs for a preprocessor that understands `jsx` syntax, which
is not simple and we believe that it has implicitly impact on those things

- it is not native javascript syntax
  - you need to learn something that isn't officially supported by TC39
  - you need to use preprocessor tool, in order:
    - to build an application (e.g. Webpack, Typescript, Babel)
    - to test application (e.g. Mocha)
    - to use static analysis tool (e.g. Prettier)
  - to use `jsx` syntax, files have to end with special extension (e.g. `.jsx`, `tsx`)
    - you need to think about it, when you set a project, build and test pipeline

Things above lead us to:

- limit selection preprocessor tools in our project to ones that understand the special syntax
- use special plugins for existing tools
  - ofter leads to node_modules pollution
  - editing config files
- big inconsistency between written and compiled code (generating minimaps has an impact on build performance)
