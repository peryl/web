---
id: hsml
title: HSML Reference
---

## Hsml Reference

HSML grammar is based on [http://www.jsonml.org/](http://www.jsonml.org/), but
to make it more consistent and type friendly (typescript recursive types) we did
several changes to jsoml structure, which later becomes HSML.

In case you really dont like HSML syntax, we recommend to use [hypescript helpers](hyperscript.md).

You can convert existing HTML structure to HSML using online converter [https://peryl.github.io/html2hsml/](https://peryl.github.io/html2hsml/)

## Grammar

```typescript
type Element =
  | [Tag]
  | [Tag, Attributes]
  | [Tag, Element[]]
  | [Tag, Attributes, Element[]];

type Tag = string;

type Attributes = {
  [attribute: string]: string;
};
```

## Examples

### 1. Empty element

Peryl automatically detects and render proper un/paired tags.

```javascript
// paired tags
["div"];
["a"];
["p"];

// unpaired tags
["input"];
["br"];
```

```html
<!-- paired tags -->
<div></div>
<a></a>
<p></p>

<!-- unpaired tags -->
<input />
<br />
```

### 2. Element with inner text

```javascript
["p", ["Hello World"]];
["b", ["Bold"]];
```

```html
<p>Hello World</p>
<b>Bold</b>
```

### 3. `class` and `id` attributes

Adding `class` and `id` attributes are similar to CSS like query selector.

```javascript
["input#inp-username"];
["input.input-text.input-big"];
["input#inp-username.input-text.input-big"];
```

```html
<input id="inp-username" />
<input class="input-text input-big" />
<input id="inp-username" class="input-text input-big" />
```

### 4. Element with attributes

```javascript
["input", { type: "text", placeholder: "Username", value: "" }];
["a", { href: "#", style: "color: red; text-decoration: 'none';" }];
["a", { href: "#" }, ["Click for more"]];
```

```html
<input type="text" placeholder="Username" value="" />
<a href="#" style="color: red; text-decoration: 'none';"></a>
<a href="#">Click for more</a>
```

### 5. Element with childrens

Children are always the last item in our `hsml` like an array.

```javascript
["ul", [
    ["li", ["Bread"]],
    ["li", ["Butter"]],
    ["li", ["Milk"]]
]];

["div", [
  ["h1", ["Hello World"]],
  ["div", [
    ["p", ["Here starts a section"]]
  ]]
]];
```

```html
<ul>
  <li>Bread</li>
  <li>Butter</li>
  <li>Milk</li>
</ul>

<div>
  <h1>Hello World</h1>
  <div>
    <p>Here starts a section</p>
  </div>
</div>
```
