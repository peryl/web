---
id: raw-html
title: Unescaped HTML
---

## Be aware

**In general, setting HTML from code is risky because it’s easy to inadvertently expose your users to a cross-site scripting (XSS) attack.**

By default, HTML rendered on server is escaped. In case you don't want to escape html in situation like rendering
javascript, use `h("script", new String("Unescaped"))` instead of `h("script", "escaped")`.

## Unescaped vs Escaped

*note: those examples only work on Server Side (NodeJS)*

### Unescaped (default) by using `""`

```typescript
import { h } from "peryl/dist/hsml-h"

function view() {
    return h("div", [
        h("script", "alert('hello world')");
    ]);
}
```

Output:

```html
<div>
    <script>alert(&#39;hello world&#39;);</script>
</div>  
```

### Escaped by using `new String("")`

```typescript
import { h } from "peryl/dist/hsml-h"

function view() {
    return h("div", [
        h("script", new String("alert('hello world')"));
    ]);
}
```

Output:

```html
<div>
    <script>alert('hello world')</script>
</div>
```


