---
id: installation
title: Installation
---

## Why ?

Peryls helps to prototype and deliver much quicker, there are no complicated concepts like in other
frameworks and also you don't need additional tooling or code preprocessors. You can
start directly with html. Peryl is based on google's library `incremental-dom`, which
automatically change DOM based on state changes.

### incremental-dom

- saves RAM, there's no VDOM
- blazing fast, diffs against actual DOM
- smaller bundle sizes, treeshaking works great

### peryl

- easy to use, templates are pure javascript object, you don't need aditional tools or preprocessors to transpile code
- easy to learn, pure `MVC`. No more complicated topics like `props`, `callbacks` nor `containers`
- server-side rendering out of box

## Using CDN

Best for begineers and also for prototyping

```html
    <script src="https://unpkg.com/peryl@1.4.32/incremental-dom/dist/umd/incremental-dom.js"></script>
    <script src="https://unpkg.com/peryl@1.4.32/dist/umd/hsml-h.js"></script>
    <script src="https://unpkg.com/peryl@1.4.32/dist/umd/hsml-app.js"></script>
```

## Using NPM

```sh
npm i peryl
```

## Using Yarn

```ssh
yarn add peryl
```
