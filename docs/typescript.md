---
id: typescript
title: Using Typescript
sidebar_label: Using Typescript
---

WIP

## 1. Installation

In order to use typescript in our project, we would need to install `peryl` package first. Peryl is written in typescript, so you **don't need** to install an additional `@types` library.

```sh
npm i peryl
```

## 2. View

```ts
// page.ts
import { HView } from "peryl/dist/hsml-app";
import { h } from "peryl/dist/hsml-h"

export interface PageState {
  txt: string;
}

export const pageState: PageState = {
  txt: ""
};

export enum PageActions {
  CHANGE_TXT = "CHANGE_TXT"
}

export const page: HView<PageState> = state => {
  return [
    h("div", [
      h("h1", "Hello Examples"),
      h("input", { value: state.txt, on: ["input", PageActions.CHANGE_TXT] }),
      h("p", "Hello " + state.txt)
    ])
  ];
};
```

## 3. Main App

```ts
// app.ts
import { HApp, HDispatcher } from "peryl/dist/hsml-app";
import { page, pageState, PageState, PageActions } from "./page";

const control: HDispatcher<PageState> = (app, action) => {
  if (action.type === PageActions.CHANGE_TXT) {
    app.txt = action.event.target.value;
    app.update();
  }
}

const app = new HApp<PageState>(pageState, page, dispatcher)
  .mount(document.getElementById("app"));
```
