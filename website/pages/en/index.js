/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

const React = require('react');

const CompLibrary = require('../../core/CompLibrary.js');

const MarkdownBlock = CompLibrary.MarkdownBlock; /* Used to read markdown */
const Container = CompLibrary.Container;
const GridBlock = CompLibrary.GridBlock;

class HomeSplash extends React.Component {
  render() {
    const { siteConfig, language = '' } = this.props;
    const { baseUrl, docsUrl } = siteConfig;
    const docsPart = `${docsUrl ? `${docsUrl}/` : ''}`;
    const langPart = `${language ? `${language}/` : ''}`;
    const docUrl = doc => `${baseUrl}${docsPart}${langPart}${doc}`;

    const SplashContainer = props => (
      <div className="homeContainer">
        <div className="homeSplashFade" style={{ backgroundColor: "#282c34" }}>
          <div className="wrapper homeWrapper" style={{
            backgroundImage: `url('${baseUrl}img/nanotechnology.svg')`,
            backgroundRepeat: "no-repeat",
            // backgroundSize: "50% auto",
            backgroundPosition: "100% 10px",
            // opacity: 0.1
          }}>
            {props.children}
          </div>
        </div>
      </div>
    );

    const Logo = props => (
      <div className="projectLogo">
        <img src={props.img_src} alt="Project Logo" />
      </div>
    );

    const ProjectTitle = () => (
      <h2 className="projectTitle" style={{ color: "white" }}>
        {siteConfig.title}
        <small>{siteConfig.tagline}</small>
      </h2>
    );

    const PromoSection = props => (
      <div className="section promoSection">
        <div className="promoRow">
          <div className="pluginRowBlock">{props.children}</div>
        </div>
      </div>
    );

    const Button = props => (
      <div className="pluginWrapper buttonWrapper">
        <a className="button big" href={props.href} target={props.target} style={{ color: "white" }}>
          {props.children}
        </a>
      </div>
    );

    return (
      <SplashContainer>
        {/* <Logo img_src={`${baseUrl}img/undraw_monitor.svg`} /> */}
        <div className="inner">
          <ProjectTitle siteConfig={siteConfig} />
          <PromoSection>
            <Button href="/web/docs/installation">Quickstart</Button>
            {/* <Button href={docUrl('doc2.html')}>Github</Button> */}
          </PromoSection>
        </div>
      </SplashContainer>
    );
  }
}

class Index extends React.Component {
  render() {
    const { config: siteConfig, language = '' } = this.props;
    const { baseUrl } = siteConfig;

    const Block = props => (
      <Container
        padding={['bottom', 'top']}
        id={props.id}
        background={props.background}>
        <GridBlock
          align="center"
          contents={props.children}
          layout={props.layout}
        />
      </Container>
    );

    const FeatureCallout = () => (
      <div
        className="productShowcaseSection paddingBottom"
        style={{ textAlign: 'center' }}>
        <h2>Feature Callout</h2>
        <MarkdownBlock>These are features of this project</MarkdownBlock>
      </div>
    );

    const TryOut = () => (
      <Block id="try">
        {[
          {
            content:
              'To make your landing page more attractive, use illustrations! Check out ' +
              '[**unDraw**](https://undraw.co/) which provides you with customizable illustrations which are free to use. ' +
              'The illustrations you see on this page are from unDraw.',
            image: `${baseUrl}img/undraw_code_review.svg`,
            imageAlign: 'left',
            title: 'Wonderful SVG Illustrations',
          },
        ]}
      </Block>
    );

    const Description = () => (
      <Block background="dark">
        {[
          {
            content:
              'This is another description of how this project is useful',
            image: `${baseUrl}img/undraw_note_list.svg`,
            imageAlign: 'right',
            title: 'Description',
          },
        ]}
      </Block>
    );

    const LearnHow = () => (
      <Block background="light">
        {[
          {
            content:
              'Each new Docusaurus project has **randomly-generated** theme colors.',
            image: `${baseUrl}img/undraw_youtube_tutorial.svg`,
            imageAlign: 'right',
            title: 'Randomly Generated Theme Colors',
          },
        ]}
      </Block>
    );

    const Features = () => (
      <Block layout="threeColumn">
        {[
          {
            content: 'There are no template languages, no need to use transpilers or preprocessor to create an application.',
            image: `${baseUrl}img/undraw_new_entries.svg`,
            imageAlign: 'top',
            title: 'No transpilers',
          },
          {
            content: 'From quick prototyping nano application to developing scalable solution.',
            image: `${baseUrl}img/undraw_select_option.svg`,
            imageAlign: 'top',
            title: 'Small, fast and reliable',
          },
          {
            content: 'Isomorphic rendering on both client and server, along with fast-booting from server-side renders.',
            image: `${baseUrl}img/undraw_server_cluster.svg`,
            imageAlign: 'top',
            title: 'Isomorphic',
          },
        ]}
      </Block>
    );

    const Interactive = () => (
      // <div className="section">
      // </div>
      <div className="container darkBackground paddingBottom paddingTop" style={{ backgroundColor: "#24292e" }}>
        <div className="wrapper">
          {/* <div className="gridBlock"> */}
          <div>
            {/* <div className="blockElement alignCenter imageAlignSide imageAlignRight twoByGridBlock"> */}
            <div className="blockElement">
              <div className="blockContent">
                <h2>
                  <div>
                    <span>
                      <p>Easy to learn</p>
                    </span>
                  </div>
                </h2>
                <p>
                  You can start developing a frontend application in Peryl just by seeing several examples.
                </p>
                <script async src="//jsfiddle.net/ateamsolutions/jt2p7s8u/embed/html,result/dark/"></script>
                {/* <h2>
                  <div>
                    <span>
                      <p>Description</p>
                    </span>
                  </div>
                </h2>
                <div>
                  <span>
                    <p>This is another description of how this project is useful</p>
                  </span>
                </div> */}
              </div>
              {/* <div className="blockImage">
                
              </div> */}
            </div>
          </div>
        </div>
      </div>
    )

    const Showcase = () => {
      if ((siteConfig.users || []).length === 0) {
        return null;
      }

      const showcase = siteConfig.users
        .filter(user => user.pinned)
        .map(user => (
          <a href={user.infoLink} key={user.infoLink}>
            <img src={user.image} alt={user.caption} title={user.caption} />
          </a>
        ));

      const pageUrl = page => baseUrl + (language ? `${language}/` : '') + page;

      return (
        <div className="productShowcaseSection paddingBottom">
          <h2>Who is Using This?</h2>
          <p>This project is used by all these people</p>
          <div className="logos">{showcase}</div>
          <div className="more-users">
            <a className="button" href={pageUrl('users.html')}>
              More {siteConfig.title} Users
            </a>
          </div>
        </div>
      );
    };

    return (
      <div>
        <HomeSplash siteConfig={siteConfig} language={language} />
        <div className="mainContainer">
          <Features />
          {/* <img src={`${baseUrl}img/53094.jpg`} width={600} /> */}
          {/* <FeatureCallout /> */}
          {/* <LearnHow /> */}
          {/* <TryOut /> */}
          {/* <Description /> */}
          {/* <Showcase /> */}
          <Interactive />
        </div>
      </div>
    );
  }
}

module.exports = Index;
