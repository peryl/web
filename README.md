# PeRyL Web

Web: https://peryl.gitlab.io/web/

PeRyL js framework repo: https://gitlab.com/peryl/peryl

## Overview

PeRyL website is build using https://docusaurus.io/ v1

## Development

`cd website`

`npm i`

`npm start`